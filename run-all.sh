#!/bin/bash

action='start'
if [ "$1" == "stop" ]; then
	action="stop"
fi

SESSION_NAME="apps"

if [ "$action" == "start" ]; then
	tmux new-session -s $SESSION_NAME -n main -d
	tmux new-window -t $SESSION_NAME -n "e9t"
    tmux new-window -t $SESSION_NAME -n "e9t-ui"
    tmux split-window -h -t $SESSION_NAME
    tmux new-window -t $SESSION_NAME -n "lh"
    tmux new-window -t $SESSION_NAME -n "aml"

    tmux send-keys -t $SESSION_NAME:2.1 'run-e9t start' c-m
    tmux send-keys -t $SESSION_NAME:3.1 'run-e9t-ui start-dev' c-m
    tmux send-keys -t $SESSION_NAME:3.2 'run-e9t-ui build-dev' c-m
    tmux send-keys -t $SESSION_NAME:4.1 'run-lh start' c-m
    tmux send-keys -t $SESSION_NAME:5.1 'run-aml start' c-m

	tmux attach

elif [ "$action" == "stop" ]; then
	tmux send-keys -t $SESSION_NAME:1.1 '' C-c
	tmux send-keys -t $SESSION_NAME:2.1 '' C-c
	tmux send-keys -t $SESSION_NAME:3.1 '' C-c
	tmux send-keys -t $SESSION_NAME:4.1 '' C-c
	tmux send-keys -t $SESSION_NAME:5.1 '' C-c

	tmux kill-session -t $SESSION_NAME
    exit;
fi
