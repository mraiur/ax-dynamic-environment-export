Dynamic environment export based on environment variables files

The script will list all files in root of **$APP_ENV_DIR_PATH** as options ( LH, AML etc).

Any subdirectory in **$APP_ENV_DIR_PATH** will be listed as option for custom environment settings ( dev, preprod, stage etc ).

# Example file : lh

```
PORT=8080
AML_LOG_FILE=filelog.log
```

# Setup

In your bash_profile set **APP_ENV_DIR_PATH** that points to folder containing your env files

```
APP_ENV_DIR_PATH="/HOME/PATH_TO_ENV_FOLDER"
```


When the script is run with **". ./app-env.sh lh"** ( lh for lighthouse ) this will search in
APP_ENV_DIR_PATH for file named **lh**  and in sub folder **dev** lh concating the variables and then exporting them.

Also for convinience will echo them.

You can specify env folder to be concatinated with third argument

```
. ./app-env.sh lh
. ./app-env.sh lh dev
. ./app-env.sh lh preprod
```

## Optional path variables

```
export WORKDIR_E9T="$HOME/work/e9t"
export WORKDIR_E9T_UI="$HOME/work/entitlements-ui"
export WORKDIR_AML="$HOME/work/analytics-mediation-layer"
export WORKDIR_LIGHTHOUSE="$HOME/work/lighthouse"
export WORKDIR_RML="$HOME/work/recurly-mediation-layer"
```

## Example aliases

!!! Tested with zsh not with bash !!!


### For dev env

```
alias run-e9t="cd $WORKDIR_E9T;. ~/scripts/app-env.sh e9t; yarn run"
alias run-e9t-ui="cd $WORKDIR_E9T_UI;. ~/scripts/app-env.sh  e9t-ui; npm run-script"
alias run-lh="cd $WORKDIR_LIGHTHOUSE;. ~/scripts/app-env.sh  lh; npm run-script"
alias run-aml="cd $WORKDIR_AML;. ~/scripts/app-env.sh  aml; npm run-script"
alias run-rml="cd $WORKDIR_RML;. ~/scripts/app-env.sh  rml; npm run-script"
```

### For preprod env

```
alias run-e9t-preprod="cd $WORKDIR_E9T;. ~/scripts/app-env.sh e9t preprod; yarn run"
alias run-e9t-ui-preprod="cd $WORKDIR_E9T_UI;. ~/scripts/app-env.sh e9t-ui preprod; npm run-script"
alias run-lh-preprod="cd $WORKDIR_LIGHTHOUSE;. ~/scripts/app-env.sh lh preprod; npm run-script"
alias run-aml-preprod="cd $WORKDIR_AML;. ~/scripts/app-env.sh aml preprod; npm run-script"
alias run-rml-preprod="cd $WORKDIR_RML;. ~/scripts/app-env.sh rml preprod; npm run-script"
```


### Quick navigation

```
alias aml="cd $WORKDIR_AML"
alias e9t="cd $WORKDIR_E9T"
alias eui="cd $WORKDIR_E9T_UI"
alias lh="cd $WORKDIR_LH"
alias rml="cd $WORKDIR_RML"
```


# Run all applications in tmux session

```
./run-all.sh
```


# (optional) Running with save( or concat ) and load in profile

When run with **save** will generate **temp.env** file that can be loaded in **bash_profile**

```
. ./app-env.sh lh preprod save
```

To load in .bash_profile add it at the bottom

```
if [ -f ~/temp.env ]; then
    . ~/temp.env
    echo 'Loaded temp.env'
fi
```

When executed with concat will persist previous temp.env vars

```
. ./app-env.sh e9t preprod concat
. ./app-env.sh lh preprod concat
```

This will have variables for e9t and lh.
