#!/bin/bash

if [[ -z ${APP_ENV_DIR_PATH} ]];
then
	APP_ENV_DIR_PATH=$HOME"/scripts"
fi

app="$1"
env="dev"

envFilesOptions=""
envDirOptions=""

for entry in "$APP_ENV_DIR_PATH/env"/*
do
	xpath=${entry%/*}
	xbase=${entry##*/}
	xfext=${xbase##*.}
	xpref=${xbase%.*}
	if [[ -f "$APP_ENV_DIR_PATH/env/$xpref" ]]; then
		if [[ ${#envFilesOptions} > 0 ]]; then
			envFilesOptions=$envFilesOptions"|"
		fi
		envFilesOptions=$envFilesOptions$xpref
	else
		if [[ ${#envDirOptions} > 0 ]]; then
			envDirOptions=$envDirOptions"|"
		fi

		envDirOptions=$envDirOptions$xpref

		if [[ "$2" == "$xpref" ]]; then
			env="$2"
		fi
	fi
done


if [[ "$app" == "" ]]; then

	echo "specify name of service to start"
	echo "./start-app.sh ($envFilesOptions) ($envDirOptions) (save|concat|echo)"

elif [[ -f "$APP_ENV_DIR_PATH/env/$env/$app" ]]; then

	vars=$(cat "$APP_ENV_DIR_PATH/env/$app" )

	if [[ -f "$APP_ENV_DIR_PATH/env/$env/$app" ]]; then
		envVars=$(cat "$APP_ENV_DIR_PATH/env/$env/$app")
		echo $envVars | while read line
		do
			varName="${line%%=*}"

			echo $vars | tr " " "\n" | while read originalValue
			do
				orgVarName="${originalValue%%=*}"
				if [[ "$varName" == "$orgVarName" ]]; then
					vars=$(sed "s|$originalValue|$line|g" <<< $vars)
				fi
			done

			if [[ $vars != *"$varName="* ]]; then
				vars=$vars"\n"$line
			fi
		done
	fi

	echo " "
	echo "Exported variables : \n$vars"

	export eval $(echo $vars | egrep -v '^#' | xargs)

	if [[ "$3" == "save" ]]; then
		echo "Saved as temp.env "
 		echo $vars | tr " " "\n" | sed 's/^/export /g' > $HOME/temp.env
 	elif [[ "$3" == "concat" ]]; then
		echo "Concat as temp.env "
		echo "\n" >> $HOME/temp.env
 		echo $vars | tr " " "\n" | sed 's/^/export /g' >> $HOME/temp.env
 	elif [[ "$3" == "echo" ]]; then
 		echo "echo \n"
		string=$(echo  $vars | tr "\n" ";")
		echo $string
 	fi
fi
